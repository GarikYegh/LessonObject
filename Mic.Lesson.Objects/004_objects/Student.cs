﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _004_objects
{
    class Student
    {
        public Student()
        {
            System.Diagnostics.Debug.WriteLine("my ctor.");
        }

        public string name;
        public string surname;
        public string email;
        public int age;
    }
}
