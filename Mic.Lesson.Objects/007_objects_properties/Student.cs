﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _007_objects_properties
{
    class Student
    {
        public Student(string email)
        {
            this.email = email;
        }

        public readonly string email;

        public string name;
        public string surname;

        public string FullName => $"{surname} {name}";
        //public string FullName
        //{
        //    get { return $"{surname} {name}"; }
        //}

        private byte age;
        public byte Age
        {
            get { return age; }
            set
            {
                if (value < 17 || value > 50)
                    age = 0;
                else
                    age = value;
            }
        }
    }
}
