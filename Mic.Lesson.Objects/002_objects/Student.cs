﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _002_objects
{
    class Student
    {
        public Student()
        {
            age = 17;
        }

        public Student(string name)
            : this()
        {
            if (char.IsLetter(name[0]))
                this.name = name;
        }

        public Student(string name, string surname)
            : this(name)
        {
            //this.name = name;
            this.surname = surname;
        }

        public Student(string name, string surname, string email)
            : this(name, surname)
        {
            //this.name = name;
            //this.surname = surname;
            this.email = email;
        }

        public string name;
        public string surname;
        public string email;
        public int age;
    }
}
